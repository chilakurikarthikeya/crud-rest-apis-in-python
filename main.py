import pymysql
from app import app
from db_config import mysql
from flask import jsonify
from flask import flash, request

import random
		
#adding product
@app.route('/add', methods=['POST'])
def add_product():
	try:
		_json = request.json
		_pname = _json['pname']
		_p_prise = _json['p_prise']
	
		# validate the received values
		if _pname and _p_prise  and request.method == 'POST':
			
			p_number=random.randint(1,1000)

			sql = "INSERT INTO products(pname, p_prise,p_id ) VALUES(%s, %s, %s)"
			data = (_pname, _p_prise, p_number)
			conn = mysql.connect()
			cursor = conn.cursor()
			cursor.execute(sql, data)
			conn.commit()
			resp = jsonify('product added successfully!')
			resp.status_code = 200
			return resp
		else:
			return not_found()
	except Exception as e:
		print(e)
	finally:
		cursor.close() 
		conn.close()
		
# display all products
@app.route('/products')
def products():
	try:
		conn = mysql.connect()
		cursor = conn.cursor(pymysql.cursors.DictCursor)
		cursor.execute("SELECT * FROM products")
		rows = cursor.fetchall()
		resp = jsonify(rows)
		resp.status_code = 200
		return resp
	except Exception as e:
		print(e)
	finally:
		cursor.close() 
		conn.close()
		
@app.route('/product/')
def product(id):
	try:
		conn = mysql.connect()
		cursor = conn.cursor(pymysql.cursors.DictCursor)
		cursor.execute("SELECT * FROM products WHERE p_id=%s", id)
		row = cursor.fetchone()
		resp = jsonify(row)
		resp.status_code = 200
		return resp
	except Exception as e:
		print(e)
	finally:
		cursor.close() 
		conn.close()

#update products
@app.route('/update', methods=['POST'])
def update_product():
	try:
		_json = request.json
		_p_id = _json['p_id']
		_pname = _json['p_name']
		_p_prise = _json['p_prise']
				
		# validate the received values
		if _pname and _p_prise  and _p_id and request.method == 'POST':
			#do not save password as a plain text
			
			# save edits
			sql = "UPDATE products SET pname=%s, p_prise=%s  WHERE p_id=%s"
			data = (_pname, _p_prise, _p_id,)
			conn = mysql.connect()
			cursor = conn.cursor()
			cursor.execute(sql, data)
			conn.commit()
			resp = jsonify('product updated successfully!')
			resp.status_code = 200
			return resp
		else:
			return not_found()
	except Exception as e:
		print(e)
	finally:
		cursor.close() 
		conn.close()

#delete product		
@app.route('/delete/')
def delete_product(id):
	try:
		conn = mysql.connect()
		cursor = conn.cursor()
		cursor.execute("DELETE FROM products WHERE p_id=%s", (id,))
		conn.commit()
		resp = jsonify('product deleted successfully!')
		resp.status_code = 200
		return resp
	except Exception as e:
		print(e)
	finally:
		cursor.close() 
		conn.close()
		
@app.errorhandler(404)
def not_found(error=None):
    message = {
        'status': 404,
        'message': 'Not Found: ' + request.url,
    }
    resp = jsonify(message)
    resp.status_code = 404

    return resp
		
if __name__ == "__main__":
    app.run()